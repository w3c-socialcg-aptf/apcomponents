import { Config } from '@stencil/core';
import nodePolyfills from 'rollup-plugin-node-polyfills';
import { sass } from '@stencil/sass';

export const config: Config = {
  namespace: 'apcomponents',
  preamble: "ActivityPub Generic Components",
  globalScript: './src/utils/global.ts',
  excludeSrc: [
    '/test/',
    '**/.spec.'
  ],
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader'
    },
    {
      type: 'docs-readme'
    },
    {
      type: 'www',
      empty: false,
      serviceWorker: null // disable service workers
    }
  ],
  plugins: [
    nodePolyfills(),
    sass({
      includePaths: [
        './node_modules'
      ]
    })
  ]
};
