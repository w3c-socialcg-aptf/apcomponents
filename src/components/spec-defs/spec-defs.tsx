import { Component, Prop, h, Method } from '@stencil/core'
import {
  ParsedClass,
  Property,
  Ontology as IOntology,
  ScrapedVocabulary
} from '../../utils/types'
import { getVocabulary } from '../../utils/scraper'

interface Named {
  name: string
}

@Component({
  tag: 'spec-defs',
  styleUrl: 'spec-defs.css',
  shadow: true
})
export class SpecDefs {
  ontologies: any = []
  as2Vocab: ScrapedVocabulary

  @Prop() url: string = "https://gitlab.com/w3c-socialcg-aptf/apcomponents/raw/cfe70e95c7a98fe506097e7154b9d36e2f7b0901/fixtures/spec.json"

  @Method()
  async getVocab() {
    return Promise.resolve(this.as2Vocab)
  }

  @Method()
  async getOntologies() {
    return Promise.resolve(this.ontologies)
  }

  async componentWillLoad() {
    this.as2Vocab = await getVocabulary(this.url)
    const ontologiesOfTypes: Array<IOntology<ParsedClass> & Named> = [
      { name: "Core Types", ...this.as2Vocab.sections.coreTypes },
      { name: "Activity Types", ...this.as2Vocab.sections.activityTypes },
      { name: "Actor Types", ...this.as2Vocab.sections.actorTypes },
      { name: "Object And Link Types", ...this.as2Vocab.sections.objectTypes }
    ]
    const ontologiesOfProperties: Array<IOntology<Property> & Named> = [
      { name: "Properties", ...this.as2Vocab.sections.properties }
    ]
    this.ontologies = [...ontologiesOfTypes, ...ontologiesOfProperties]
  }

  render() {
    return <div>
      {this.ontologies.map(o =>
        <span>
          <h2>{o.name}</h2>
          {o.members.map(member =>
            <span>
              <dt><a href={member.url}>{member.name}</a></dt>
              <dd>{member.notes}</dd>
            </span>
          )}
        </span>
      )}
    </div>;
  }
}
