# spec-defs



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type     | Default                                                                                                               |
| -------- | --------- | ----------- | -------- | --------------------------------------------------------------------------------------------------------------------- |
| `url`    | `url`     |             | `string` | `"https://gitlab.com/w3c-socialcg-aptf/apcomponents/raw/cfe70e95c7a98fe506097e7154b9d36e2f7b0901/fixtures/spec.json"` |


## Methods

### `getOntologies() => Promise<any>`



#### Returns

Type: `Promise<any>`



### `getVocab() => Promise<ScrapedVocabulary>`



#### Returns

Type: `Promise<ScrapedVocabulary>`




## Dependencies

### Used by

 - [spec-card](../spec-card)

### Graph
```mermaid
graph TD;
  spec-card --> spec-defs
  style spec-defs fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
