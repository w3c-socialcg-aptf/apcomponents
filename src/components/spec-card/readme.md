# spec-card



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type         | Default                                                                                                                                                          |
| -------- | --------- | ----------- | ------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `config` | --        |             | `CardConfig` | `{     title: '',     subtitle: '',     description: '',     url: '',     projectUrl: '',     hasDiscussion: '',     hasRecommendation: [       'v1'     ],   }` |
| `width`  | `width`   | Card width  | `string`     | `'350px'`                                                                                                                                                        |


## Events

| Event               | Description | Type               |
| ------------------- | ----------- | ------------------ |
| `discussionClicked` |             | `CustomEvent<any>` |


## Dependencies

### Depends on

- [spec-defs](../spec-defs)

### Graph
```mermaid
graph TD;
  spec-card --> spec-defs
  style spec-card fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
