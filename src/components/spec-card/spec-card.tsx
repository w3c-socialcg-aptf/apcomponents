import { Component, h, Prop, Event, EventEmitter, State } from '@stencil/core'
import {MDCMenu} from '@material/menu'
import {MDCDialog} from '@material/dialog'
import jsondiffpatch, { Formatter } from 'jsondiffpatch'
import { getVocabulary } from '../../utils/scraper'

const spec = "https://raw.githubusercontent.com/gobengo/activitystreams2-spec-scraped/master/data/activitystreams-vocabulary/1528589057.json"

export interface CardConfig {
	title: string
	subtitle?: string
  url?: string // url of the spec
  projectUrl?: string
  description?: string
  hasDiscussion: string // url of the discussion
  hasRecommendation: string[]
}
interface NewFormatter extends Formatter {
  hideUnchanged: Function
}

const jsondiff = (jsondiffpatch as any).create({
  // Define an object hash function
  objectHash: (obj, _) => obj.id || JSON.stringify(obj),
  // Setting matchByPosition as a fallback for when objectHash returns undefined can create smaller diffs
  matchByPosition: true,
})

@Component({
  tag: 'spec-card',
  styleUrls: {
    default: 'spec-card.scss',
    dark: 'spec-card.dark.scss'
  },
  shadow: true
})
export class SpecCard {
  @Prop() config: CardConfig = {
    title: '',
    subtitle: '',
    description: '',
    url: '',
    projectUrl: '',
    hasDiscussion: '',
    hasRecommendation: [
      'v1'
    ],
  }
  /**
   * Card width
   */
  @Prop() width: string = '350px'

  @State() propertiesCount = null
  menuEl: HTMLElement
  menu = null
  diffEl: HTMLElement
  diff = null
  specEl: HTMLElement
  spec = null
  left = {}
  right = {}
  diffInner: HTMLElement

  @Event() discussionClicked: EventEmitter
  discussionClickedHandler() {
    this.discussionClicked.emit()
  }

  // Lifecycles
  async componentDidLoad() {
    this.menu = new MDCMenu(this.menuEl)
    this.diff = new MDCDialog(this.diffEl)
    this.spec = new MDCDialog(this.specEl)

    this.left = await getVocabulary(spec)
    this.right = this.config.url ? await getVocabulary(this.config.url) : this.left
    const j = jsondiffpatch.formatters.html as any as NewFormatter
    j.hideUnchanged()
    const diff = jsondiff.diff(this.left, this.right)
    this.diffInner.innerHTML = j.format(diff, this.left)
    
    this.propertiesCount = Object.keys(jsondiff.diff(this.left, this.right).sections.properties.members).slice(0, -1).length
  }

  // Element toggles
  menuToggle() {
    this.menu.open = !this.menu.open
  }
  diffToggle() {
    this.diff.isOpen
      ? this.diff.close()
      : this.diff.open()
  }
  specToggle() {
    this.spec.isOpen
    ? this.spec.close()
    : this.spec.open()
  }

  // Element renderer
  render() {
    return <div>
      <div class="mdc-card" style={{ "width": this.width }}>

        <slot name="head">
          <a
            href={this.config.hasDiscussion}
            class="mdc-card__primary-action mdc-ripple-upgraded" 
            tabindex="0"
          >
            <div class="card__primary">
              <h2 class="card__title mdc-typography mdc-typography--headline6">
                {this.config.title} 
                {this.config.hasRecommendation.length > 0
                  ? <i class="material-icons card__title__icon green" title="This extension has been verified by the community">check_circle_outline</i>
                  : ''
                }
              </h2>
              <h3 class="card__subtitle mdc-typography mdc-typography--subtitle2">
                {this.config.subtitle
                  ? this.config.subtitle
                  : `${this.propertiesCount || 0} ${this.propertiesCount && this.propertiesCount > 1 ? 'properties' : 'property'} modified`
                }
              </h3>
            </div>
            <div class="card__secondary mdc-typography mdc-typography--body2">
              <slot>{this.config.description}</slot>
            </div>
          </a>
        </slot>

        <slot name="actions">
          <div class="mdc-card__actions">
            <div class="mdc-card__action-buttons">
              <button 
                onClick={_ => this.diffToggle()}
                class="mdc-button mdc-card__action mdc-card__action--button"
                title="See the difference with the latest ActivityPub specification"
              >
                <i class="material-icons mdc-button__icon">call_split</i>
                <span class="mdc-button__label">DIFF</span>
              </button>
              <button 
                onClick={_ => this.specToggle()}
                class="mdc-button mdc-card__action mdc-card__action--button"
                title="See the extension proposal in full"
              >
                <i class="material-icons mdc-button__icon">menu_books</i>
                <span class="mdc-button__label">SPEC</span>
              </button>
            </div>
            <div class="mdc-card__action-icons">
              <div class="mdc-menu-surface--anchor">
                <button
                  onClick={_ => this.menuToggle()}
                  class="material-icons mdc-icon-button mdc-card__action mdc-card__action--icon"
                  title="More options"
                >more_vert</button>
                <div ref={(el) => this.menuEl = el as HTMLElement} class="mdc-menu mdc-menu-surface">
                  <ul class="mdc-list" role="menu" aria-hidden="true" aria-orientation="vertical" tabindex="-1">
                    {this.config.hasDiscussion
                      ? <li class="mdc-list-item" role="menuitem">
                          <span class="mdc-list-item__text">
                            <i class="material-icons card__title__icon mr2">forum</i> Go to the extensions discussion
                          </span>
                        </li>
                      : ''
                    }
                    <li class="mdc-list-item" role="menuitem">
                      <a href={this.config.projectUrl} class="mdc-list-item__text" target="_blank">
                        <i class="material-icons card__title__icon mr2">launch</i> Go to project page
                      </a>
                    </li>
                    <li class="mdc-list-item" role="menuitem">
                      <span class="mdc-list-item__text">
                        <i class="material-icons card__title__icon mr2">link</i> Copy namespace URI
                      </span>
                    </li>
                  </ul>
                </div>
              </div>

            </div>
          </div>
        </slot>

        <div
          ref={(el) => this.diffEl = el as HTMLElement}
          class="mdc-dialog mdc-dialog--scrollable"
          role="alertdialog"
          aria-modal="true"
          aria-labelledby="mdc-dialog-with-list-label"
          aria-describedby="mdc-dialog-with-list-description"
        >
          <div class="mdc-dialog__scrim"></div>
          <div class="mdc-dialog__container">
            <div class="mdc-dialog__surface">
              <h2 id="mdc-dialog-with-list-label" class="mdc-dialog__title">Diff: ActivityPub spec. vs {this.config.title}</h2>
              <section id="mdc-dialog-with-list-description" class="mdc-dialog__content">
                <span ref={(el) => this.diffInner = el as HTMLElement}></span>
              </section>
              <footer class="mdc-dialog__actions">
                <button type="button" class="mdc-button mdc-dialog__button mdc-ripple-upgraded" data-mdc-dialog-action="close">Close</button>
              </footer>
            </div>
          </div>
        </div>

        <div
          ref={(el) => this.specEl = el as HTMLElement}
          class="mdc-dialog mdc-dialog--scrollable"
          role="alertdialog"
          aria-modal="true"
          aria-labelledby="mdc-dialog-with-list-label"
          aria-describedby="mdc-dialog-with-list-description"
        >
          <div class="mdc-dialog__scrim"></div>
          <div class="mdc-dialog__container">
            <div class="mdc-dialog__surface">
              <h2 id="mdc-dialog-with-list-label" class="mdc-dialog__title">Diff: ActivityPub spec. vs {this.config.title}</h2>
              <section id="mdc-dialog-with-list-description" class="mdc-dialog__content">
                <spec-defs url={this.config.url} />
              </section>
              <footer class="mdc-dialog__actions">
                <button type="button" class="mdc-button mdc-dialog__button mdc-ripple-upgraded" data-mdc-dialog-action="close">Close</button>
              </footer>
            </div>
          </div>
        </div>

      </div>
    </div>;
  }
}
