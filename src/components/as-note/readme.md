# as-note



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute        | Description                                                                                                                                                                                                                                                                                                                                      | Type      | Default                                 |
| -------------- | ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------- | --------------------------------------- |
| `activity`     | --               | Note object in ActivityPub linguo, with the `attributedTo` parameter  resolved to the Person object it represents.                                                                                                                                                                                                                               | `Note`    | `undefined`                             |
| `canFavorite`  | `can-favorite`   | Boolean, to enable the corresponding button.                                                                                                                                                                                                                                                                                                     | `boolean` | `undefined`                             |
| `canReply`     | `can-reply`      | Boolean, to enable the corresponding button.                                                                                                                                                                                                                                                                                                     | `boolean` | `undefined`                             |
| `canShare`     | `can-share`      | Boolean, to enable the corresponding button.                                                                                                                                                                                                                                                                                                     | `boolean` | `undefined`                             |
| `corsProxyUrl` | `cors-proxy-url` |                                                                                                                                                                                                                                                                                                                                                  | `string`  | `"https://cors-anywhere.herokuapp.com"` |
| `entity`       | `entity`         | Name of the entity. Default to being called a `note` (you could want to call that a `toot` or a `status`, for instance).                                                                                                                                                                                                                         | `string`  | `'note'`                                |
| `expanded`     | `expanded`       | Boolean, to expand the note in case it is too long. (defaults to false)                                                                                                                                                                                                                                                                          | `boolean` | `false`                                 |
| `noAction`     | `no-action`      | Boolean, that switches the bottom row to a format appropriate for display without any action (i.e. for public display of a Note). (defaults to true)                                                                                                                                                                                             | `boolean` | `true`                                  |
| `url`          | `url`            | URL to the Note object. Will send a proper Accept reader along with the request for the object, and resolve the underlying Person author in the `attributedTo` field. Overriden by `activity` if present.                                                                                                                                        | `string`  | `undefined`                             |
| `useCorsProxy` | `use-cors-proxy` | In case the resource is not served by a domain you control (it should!) then you probably will hit a CORS denial. It's normal. You can decide to use a proxy, but that will only get you so far with numerous visits, and the one provided by default could go down anytime. (you can host yours  with https://github.com/Rob--W/cors-anywhere/) | `boolean` | `false`                                 |
| `width`        | `width`          | Card width                                                                                                                                                                                                                                                                                                                                       | `string`  | `'350px'`                               |


## Events

| Event      | Description | Type               |
| ---------- | ----------- | ------------------ |
| `favorite` |             | `CustomEvent<any>` |
| `reply`    |             | `CustomEvent<any>` |
| `share`    |             | `CustomEvent<any>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
