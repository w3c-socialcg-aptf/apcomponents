import { Component, Host, h, Prop, Event, EventEmitter, Watch } from '@stencil/core'
import { MDCMenu } from '@material/menu'
import { Note, ASObject } from 'activitystreams2'
import { getNote } from '../../utils/scraper'
import twas from 'twas'

export interface Document {
  type: "Document",
  mediaType: string,
  url: string,
  name: string,
  blurhash: string
}

@Component({
  tag: 'as-note',
  styleUrls: {
    default: 'as-note.scss',
    dark: 'as-note.dark.scss'
  },
  shadow: true
})
export class AsNote {
  /**
   * Note object in ActivityPub linguo, with the `attributedTo` parameter 
   * resolved to the Person object it represents.
   */
  @Prop({ mutable: true }) activity: Note
  /**
   * URL to the Note object. Will send a proper Accept reader along with
   * the request for the object, and resolve the underlying Person author
   * in the `attributedTo` field. Overriden by `activity` if present.
   */
  @Prop() url: string
  /**
   * In case the resource is not served by a domain you control (it should!)
   * then you probably will hit a CORS denial. It's normal. You can decide to
   * use a proxy, but that will only get you so far with numerous visits, and
   * the one provided by default could go down anytime. (you can host yours 
   * with https://github.com/Rob--W/cors-anywhere/)
   */
  @Prop() useCorsProxy: boolean = false
  @Prop() corsProxyUrl: string = "https://cors-anywhere.herokuapp.com"
  /**
   * Name of the entity. Default to being called a `note` (you could want
   * to call that a `toot` or a `status`, for instance).
   */
  @Prop() entity: string = 'note'
  /**
   * Boolean, to enable the corresponding button.
   */
  @Prop() canReply: boolean
  /**
   * Boolean, to enable the corresponding button.
   */
  @Prop() canShare: boolean
  /**
   * Boolean, to enable the corresponding button.
   */
  @Prop() canFavorite: boolean
  /**
   * Boolean, to expand the note in case it is too long. (defaults to false)
   */
  @Prop({ reflect: true, mutable: true }) expanded: boolean = false
  /**
   * Boolean, that switches the bottom row to a format appropriate for display
   * without any action (i.e. for public display of a Note). (defaults to true)
   */
  @Prop() noAction: boolean = true
  /**
   * Card width
   */
  @Prop() width: string = '350px'

  menuEl: HTMLElement
  menu = null
  zoom = null

  // Lifecycles
  async componentWillLoad() {
    if (this.url && !this.activity) this.activity = this.useCorsProxy ? (await getNote(this.url, this.corsProxyUrl) as Note) : (await getNote(this.url) as Note)
    console.log(this.activity)
  }
  async componentDidLoad() {
    if (this.menuEl) this.menu = new MDCMenu(this.menuEl)
  }
  @Watch('url')
  async watchHandler(value: string, _) {
    if (value) this.activity = this.useCorsProxy ? (await getNote(value, this.corsProxyUrl) as Note) : (await getNote(value) as Note)
    console.log(this.activity)
  }

  // Events
  @Event() reply: EventEmitter
  replyHandler() { this.reply.emit() }
  @Event() share: EventEmitter
  shareHandler() { this.share.emit() }
  @Event() favorite: EventEmitter
  favoriteHandler() { this.favorite.emit() }

  // Element toggles
  menuToggle() {
    this.menu.open = !this.menu.open
  }
  expandedToggle() {
    this.expanded = !this.expanded
  }

  // Element renderer
  render() {
    return (
      <Host>
        <slot></slot>

        {this.activity
          ? <div class="mdc-card" style={{ "width": this.width }}>

              <slot name="head">
                <div
                  class="mdc-card__primary mdc-ripple-upgraded" 
                  tabindex="0"
                >
                  <div class="card__primary card__primary--flex avatar-parent">
                    <a class="avatar" href={((this.activity.attributedTo as ASObject).url as string)} target="_blank">
                      <img
                        src={(this.activity.attributedTo as any).icon.url}
                        alt="avatar"/>
                    </a>
                    <a class="avatar-side" href={((this.activity.attributedTo as ASObject).url as string)} target="_blank">
                      <div class="one-line-ellipsis" title={(this.activity.attributedTo as ASObject).name}>{(this.activity.attributedTo as ASObject).name}</div>
                      <div class="one-line-ellipsis mdc-typography mdc-typography--subtitle2" title={((this.activity.attributedTo as ASObject).url as string)}>{((this.activity.attributedTo as ASObject).url as string).replace('https://', '')}</div>
                    </a>
                    <div class="card-status">
                      {this.activity.inReplyTo
                        ? <i class="material-icons" title={`This ${this.entity} is a reply`}>reply</i>
                        : ''
                      }
                      {this.activity.to === "https://www.w3.org/ns/activitystreams#Public" || (this.activity.to as Array<string>).find(e => e === "https://www.w3.org/ns/activitystreams#Public")
                        ? <i class="material-icons" title={`This ${this.entity} is public`}>public</i>
                        : <i class="material-icons" title={`This ${this.entity} is private`}>email</i>
                      }
                      {this.expanded
                        ? <button
                            onClick={() => this.expandedToggle()}
                            class="material-icons mdc-icon-button mdc-icon-button-small mdc-card__action mdc-card__action--icon"
                            title={`This ${this.entity} is expanded. Click to shrink.`}
                          >expand_less</button>
                        : <button
                            onClick={() => this.expandedToggle()}
                            class="material-icons mdc-icon-button mdc-icon-button-small mdc-card__action mdc-card__action--icon"
                            title={`This ${this.entity} has more content. Click to expand.`}
                          >expand_more</button>
                      }
                    </div>
                  </div>

                  {(this.activity as any).sensitive && !this.expanded
                    ? <div class="card__secondary--sensitive">
                        <span>{this.activity.summary}</span>
                        <button onClick={() => this.expandedToggle()} class="mdc-button mdc-button--dense"><span>Show</span><i class="material-icons mdc-button__icon">image</i></button>
                      </div>
                    : <div>
                        <div class={`card__secondary mdc-typography mdc-typography--body2 ${this.expanded || this.activity.content.length < 200 ? '' : 'collapsed'}`}>
                          <slot>
                            {this.activity.content
                              ? <span class="card__secondary--content" innerHTML={this.activity.content}></span>
                              : ''
                            }
                          </slot>
                        </div>

                        {(this.activity.attachment as Array<Document>).find(a => a.mediaType.includes("image/"))
                          ? <div class={{
                              'media-gallery': true,
                              'media-gallery--single': (this.activity.attachment as Array<Document>).filter(a => a.mediaType.includes("image/")).length == 1
                            }}>
                              {(this.activity.attachment as Array<Document>).filter(a => a.mediaType.includes("image/")).map(a =>
                                <div class="mdc-card__media media-gallery__item">
                                  <img src={a.url}/>
                                </div>
                              )}
                            </div>
                          : ''
                        }
                      </div>
                  }
                </div>
              </slot>

              <slot name="actions">
                {this.noAction
                  ? <div class="mdc-card__actions no-actions">
                      <div class="mdc-card__action-buttons">
                        <a class="undercorated" href={(this.activity.url as string) || (this.activity.id as string)} target="_blank"
                          title={(new Date(this.activity.published).toLocaleString())}
                        >
                          {(new Date(this.activity.published))['toGMTString']()}
                        </a>
                      </div>
                      {this.activity.inReplyTo ? <span> · </span> : ''}
                      {this.activity.inReplyTo
                        ? <a
                            href={(this.activity.inReplyTo as string)}
                            class="material-icons mdc-icon-button mdc-card__action mdc-card__action--icon"
                            title="See replies"
                          >reply_all</a>
                        : ''
                      }
                    </div>
                  : <div class="mdc-card__actions">
                      <div class="mdc-card__action-buttons">
                        <button
                          onClick={_ => this.replyHandler()}
                          class="material-icons mdc-icon-button mdc-card__action mdc-card__action--icon"
                          title="Reply"
                          disabled={!this.canReply}
                        >reply</button>
                        <button
                          onClick={_ => this.shareHandler()}
                          class="material-icons mdc-icon-button mdc-card__action mdc-card__action--icon"
                          title="Share"
                          disabled={!this.canShare}
                        >repeat</button>
                        <button
                          onClick={_ => this.favoriteHandler()}
                          class="material-icons mdc-icon-button mdc-card__action mdc-card__action--icon"
                          title="Favorite"
                          disabled={!this.canFavorite}
                        >star</button>
                        <div class="mdc-menu-surface--anchor">
                          <button
                            onClick={_ => this.menuToggle()}
                            class="material-icons mdc-icon-button mdc-card__action mdc-card__action--icon"
                            title="More options"
                          >more_horizontal</button>
                          <div ref={(el) => this.menuEl = el as HTMLElement} class="mdc-menu mdc-menu-surface">
                            <ul class="mdc-list" role="menu" aria-hidden="true" aria-orientation="vertical" tabindex="-1">
                              <li class="mdc-list-item" role="menuitem">
                                <a href={(this.activity.url as string)} class="mdc-list-item__text" target="_blank">
                                  {`Unfold ${this.entity}`}
                                </a>
                              </li>
                              <li class="mdc-list-divider" role="separator"></li>
                              <li class="mdc-list-item" role="menuitem"><a href="" class="mdc-list-item__text">
                                Hide @cwebber
                              </a></li>
                              <li class="mdc-list-item" role="menuitem"><a href="" class="mdc-list-item__text">
                                Block @cwebber
                              </a></li>
                              <li class="mdc-list-item" role="menuitem"><a href="" class="mdc-list-item__text">
                                Report @cwebber
                              </a></li>
                              <li class="mdc-list-divider" role="separator"></li>
                              <li class="mdc-list-item" role="menuitem"><a href="" class="mdc-list-item__text">
                                {`Open moderation interface for this ${this.entity}`}
                              </a></li>
                              <li class="mdc-list-item" role="menuitem"><a href="" class="mdc-list-item__text">
                                Open moderation interface for this user
                              </a></li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div class="mdc-card__action-icons mdc-card__action--text mdc-typography--caption">
                        <a class="undercorated" href={(this.activity.url as string) || (this.activity.id as string)} target="_blank"
                          title={(new Date(this.activity.published).toLocaleString())}
                        >
                          {twas((new Date(this.activity.published)).getTime())}
                        </a>
                      </div>
                    </div>
                }
              </slot>

            </div>
          : <div class="mdc-card">
              <div
                class="mdc-card__primary mdc-ripple-upgraded mdc-card__primary--loading" 
                tabindex="0"
              >
                <div class="card__primary card__primary--loading">
                  The card is loading
                </div>
              </div>
            </div>
        }
      </Host>
    )
  }

}
