import { ScrapedVocabulary } from "./types"
import { ASObject, Note } from "activitystreams2"

const fetchHtml = async (url: string) => {
  const response = await fetch(url)
    .then(function(response: Response) {
      if (response.status !== 200) {
        console.log(
          "Looks like there was a problem. Status Code: " + response.status
        )
        return
      }

      // Examine the text in the response
      return response.json()
    })
    .catch(function(err) {
      console.log("Fetch Error :-S", err)
		})
	return response
}

export async function getVocabulary(url: string): Promise<ScrapedVocabulary> {
  const text = (await fetchHtml(url))
  return text
}

export async function getObject(url: string): Promise<ASObject> {
  const response = await fetch(url, {
    headers: new Headers({
      'Accept': 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
    })
  })
    .then(function(response: Response) {
      if (response.status !== 200) {
        console.log(
          "Looks like there was a problem. Status Code: " + response.status
        )
        return
      }

      // Examine the text in the response
      return response.json()
    })
    .catch(function(err) {
      console.log("AS Object Fetch Error :-S", err)
		})
	return response
}

export async function getNote(url: string, proxyUrl?: string): Promise<ASObject> {
  let note: Note = (await getObject(proxyUrl ? `${proxyUrl}/${url}` : url) as Note)
  
  let person_id: string | Array<ASObject> = note.attributedTo || (note as any).actor
  if (person_id instanceof Array) {
    person_id = person_id.find(obj => obj.type === 'Person').id
  }

  const person: ASObject = await getObject(proxyUrl ? `${proxyUrl}/${person_id}` : person_id)
  note.attributedTo = person
  return note
}