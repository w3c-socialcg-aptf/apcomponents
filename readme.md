# APComponents
APComponents is a collection of web components factorizing common and useful UI components making use of the ActivityStreams vocabulary.

## Using this component
Components are built using [StencilJS](https://stenciljs.com/) and published under `@w3c-socialcg-aptf/apcomponents` on NPM.

### Script tag
- Put a script tag: `<script src='https://unpkg.com/@w3c-socialcg-aptf/apcomponents'></script>` in the head of your index.html
- Then you can use the element anywhere in your template, JSX, html etc

### Node Modules
- Run `npm install @w3c-socialcg-aptf/apcomponents --save`
- Put a script tag similar to this `<script src='node_modules/@w3c-socialcg-aptf/apcomponents/dist/mycomponent.js'></script>` in the head of your index.html
- Then you can use the element anywhere in your template, JSX, html etc

### In a stencil-starter app
- Run `npm install @w3c-socialcg-aptf/apcomponents --save`
- Add an import to the npm packages `import @w3c-socialcg-aptf/apcomponents;`
- Then you can use the element anywhere in your template, JSX, html etc

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
